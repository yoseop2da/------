//
//  CountChecker.h
//  PushUpTracker
//
//  Created by parkyoseop on 2016. 3. 2..
//  Copyright © 2016년 yoseop2da. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountChecker : NSObject

+ (id)sharedInstance;

- (int)getCount;
- (void)addCount;
- (void)resetCount;
- (void)saveCount;

@end
