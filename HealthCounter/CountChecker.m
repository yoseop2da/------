//
//  CountChecker.m
//  PushUpTracker
//
//  Created by parkyoseop on 2016. 3. 2..
//  Copyright © 2016년 yoseop2da. All rights reserved.
//

#import "CountChecker.h"
@interface CountChecker()
{
    int _count;
}
@end

@implementation CountChecker

+ (id)sharedInstance
{
    static dispatch_once_t p = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

-(instancetype)init
{
    if (self = [super init]){
        _count = 0;
    }
    return self;
}

- (int)getCount
{
    return _count;
}

- (void)addCount
{
    ++_count;
}

- (void)resetCount
{
    _count = 0;
}

- (void)saveCount
{
    NSLog(@"Save Count : %d",_count);
}

@end
