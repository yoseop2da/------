//
//  DataManager.h
//  PushUpTracker
//
//  Created by parkyoseop on 2016. 3. 2..
//  Copyright © 2016년 yoseop2da. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, HistoryType) {
    HistoryTypeDate,
    HistoryTypeCount
};

@interface DataManager : NSObject

@property (assign, nonatomic) HistoryType historyType;

+ (id)sharedInstance;


- (void)setHistoryType:(HistoryType)historyType;
- (NSDictionary *)getCountHistory;
- (NSDictionary *)getDateHistory;
- (void)saveHistoryWithCount:(int)count;

- (void)removeHistoryWithKey:(NSString *)key;

- (NSString *)simPleKeyFromFullKey:(NSString *)fullKey;
@end
