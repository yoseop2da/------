//
//  DataManager.m
//  PushUpTracker
//
//  Created by parkyoseop on 2016. 3. 2..
//  Copyright © 2016년 yoseop2da. All rights reserved.
//

#import "DataManager.h"

@interface DataManager()
{
    NSMutableDictionary *_history;
}
@end

@implementation DataManager

+ (id)sharedInstance
{
    static dispatch_once_t p = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

-(instancetype)init
{
    if (self = [super init]) {

        NSDictionary *dic = [[NSUserDefaults standardUserDefaults] objectForKey:HISTORY_KEY];
        if (dic) {
            _history = [dic mutableCopy];
        }else{
            _history = [NSMutableDictionary dictionary];
        }
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:TYPE_KEY]){
            _historyType = HistoryTypeDate;
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:TYPE_KEY];
        }else{
            _historyType = HistoryTypeCount;
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:TYPE_KEY];
        }
        
    }
    return self;
}

- (void)setHistoryType:(HistoryType)historyType
{
    _historyType = historyType;
}

- (NSDictionary *)getCountHistory
{
    return _history;
}

- (NSDictionary *)getDateHistory
{
    NSMutableDictionary *tempDic = [NSMutableDictionary dictionary];
    
    // key = yyyy.mm.dd.hh.mm.ss
    for (NSString *key in _history.allKeys) {
        NSString *tempKey = [self simPleKeyFromFullKey:key];
        if (tempDic[tempKey]) {
            [tempDic setObject:@([tempDic[tempKey] intValue] + [_history[key] intValue]) forKey:tempKey];
        }else{
            [tempDic setObject:_history[key] forKey:tempKey];
        }
    }
    return tempDic;
}

- (void)saveHistoryWithCount:(int)count
{
    [_history setObject:@(count) forKey:[self keyFromDate:[NSDate date]]];
    
    
    [_history setObject:@(3) forKey:@"2016.03.03.14.55.24"];
    [_history setObject:@(5) forKey:@"2016.03.04.15.55.24"];
    [_history setObject:@(8) forKey:@"2016.03.05.16.55.24"];
    [_history setObject:@(11) forKey:@"2016.03.06.17.55.24"];
    [_history setObject:@(30) forKey:@"2016.03.07.18.55.24"];
    [_history setObject:@(19) forKey:@"2016.03.08.19.55.24"];
    [_history setObject:@(7) forKey:@"2016.03.09.3.55.24"];
    [_history setObject:@(30) forKey:@"2016.03.10.18.55.24"];
    [_history setObject:@(19) forKey:@"2016.03.11.19.55.24"];
    [_history setObject:@(7) forKey:@"2016.03.12.3.55.24"];
    [_history setObject:@(7) forKey:@"2016.03.13.3.55.24"];
    [_history setObject:@(7) forKey:@"2016.03.14.3.55.24"];
    [_history setObject:@(7) forKey:@"2016.03.15.3.55.24"];
    
    
    [_history setObject:@(3) forKey:@"2016.03.03.14.56.24"];
    [_history setObject:@(5) forKey:@"2016.03.04.15.57.24"];
    [_history setObject:@(8) forKey:@"2016.03.05.16.56.24"];
    [_history setObject:@(11) forKey:@"2016.03.06.17.56.24"];
    [_history setObject:@(30) forKey:@"2016.03.07.18.56.24"];
    [_history setObject:@(19) forKey:@"2016.03.08.19.56.24"];
    [_history setObject:@(7) forKey:@"2016.03.09.3.56.24"];
    [_history setObject:@(30) forKey:@"2016.03.10.18.56.24"];
    [_history setObject:@(19) forKey:@"2016.03.11.19.56.24"];
    [_history setObject:@(7) forKey:@"2016.03.12.3.56.24"];
    [_history setObject:@(7) forKey:@"2016.03.13.3.56.24"];
    [_history setObject:@(7) forKey:@"2016.03.14.3.56.24"];
    [_history setObject:@(7) forKey:@"2016.03.15.3.56.24"];
    
    
    [_history setObject:@(3) forKey:@"2016.03.03.14.57.24"];
    [_history setObject:@(5) forKey:@"2016.03.04.15.57.24"];
    [_history setObject:@(8) forKey:@"2016.03.05.16.57.24"];
    [_history setObject:@(11) forKey:@"2016.03.06.17.57.24"];
    [_history setObject:@(30) forKey:@"2016.03.07.18.57.24"];
    [_history setObject:@(19) forKey:@"2016.03.08.19.57.24"];
    [_history setObject:@(7) forKey:@"2016.03.09.3.57.24"];
    [_history setObject:@(30) forKey:@"2016.03.10.18.57.24"];
    [_history setObject:@(19) forKey:@"2016.03.11.19.57.24"];
    [_history setObject:@(7) forKey:@"2016.03.12.3.57.24"];
    [_history setObject:@(7) forKey:@"2016.03.13.3.57.24"];
    [_history setObject:@(7) forKey:@"2016.03.14.3.57.24"];
    [_history setObject:@(7) forKey:@"2016.03.15.3.57.24"];
    
    [_history setObject:@(3) forKey:@"2016.03.03.14.58.24"];
    [_history setObject:@(5) forKey:@"2016.03.04.15.58.24"];
    [_history setObject:@(8) forKey:@"2016.03.05.16.58.24"];
    [_history setObject:@(11) forKey:@"2016.03.06.17.58.24"];
    [_history setObject:@(30) forKey:@"2016.03.07.18.58.24"];
    [_history setObject:@(19) forKey:@"2016.03.08.19.58.24"];
    [_history setObject:@(7) forKey:@"2016.03.09.3.58.24"];
    [_history setObject:@(30) forKey:@"2016.03.10.18.58.24"];
    [_history setObject:@(19) forKey:@"2016.03.11.19.58.24"];
    [_history setObject:@(7) forKey:@"2016.03.12.3.58.24"];
    [_history setObject:@(7) forKey:@"2016.03.13.3.58.24"];
    [_history setObject:@(7) forKey:@"2016.03.14.3.58.24"];
    [_history setObject:@(7) forKey:@"2016.03.15.3.58.24"];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:_history forKey:HISTORY_KEY];
}

- (void)removeHistoryWithKey:(NSString *)key
{
    if (_historyType == HistoryTypeDate) {
        NSMutableDictionary *tempDic = [NSMutableDictionary dictionary];

        for (NSString *historyKey in _history.allKeys) {
            NSString *currentKey = [self simPleKeyFromFullKey:key];
            NSString *simpleKey = [self simPleKeyFromFullKey:historyKey];
            if ([simpleKey isEqualToString:currentKey]) {
            }else{
                [tempDic setObject:_history[historyKey] forKey:historyKey];
            }
        }
        _history = [tempDic mutableCopy];
    }else{
        [_history removeObjectForKey:key];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:_history forKey:HISTORY_KEY];
    
}

- (NSString *)simPleKeyFromFullKey:(NSString *)fullKey
{
    NSArray *arr = [fullKey componentsSeparatedByString:@"."];
    if (arr.count > 0) {
        return [NSString stringWithFormat:@"%@.%@.%@",arr[0],arr[1],arr[2]];
    }else{
        return fullKey;
    }
}
- (NSString *)keyFromDate:(NSDate *)date
{
    return [NSString stringWithFormat:@"%ld.%02ld.%02ld.%02ld.%02ld.%02ld",date.year, date.month, date.day, date.hour, date.minute, date.seconds];
}

@end
