//
//  HistoryTableViewCell.m
//  PushUpTracker
//
//  Created by parkyoseop on 2016. 3. 2..
//  Copyright © 2016년 yoseop2da. All rights reserved.
//

#import "HistoryTableViewCell.h"
@interface HistoryTableViewCell()
{
    IBOutlet UILabel *mainLabel;
    IBOutlet UILabel *subLabel;
    
}

@end

@implementation HistoryTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setMainText:(NSString *)mainText subText:(NSString *)subText
{
    mainLabel.text = mainText;
    subLabel.text = subText;
}

@end
