//
//  HistoryViewController.m
//  PushUpTracker
//
//  Created by parkyoseop on 2016. 3. 2..
//  Copyright © 2016년 yoseop2da. All rights reserved.
//

#import "HistoryViewController.h"
#import "HistoryTableViewCell.h"

@import GoogleMobileAds;

@interface HistoryViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    NSDictionary *_history;
    
    IBOutlet NSLayoutConstraint *dateWidth;
    IBOutlet NSLayoutConstraint *countWidth;
    
    IBOutlet GADBannerView *bannerView;
}
@property (weak, nonatomic) IBOutlet UITableView *historyTableView;

@end

@implementation HistoryViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    if ([[DataManager sharedInstance] historyType] == HistoryTypeDate) {
        dateWidth.constant = 60;
        countWidth.constant = 0;
        _history = [[DataManager sharedInstance] getDateHistory];
    }else{
        dateWidth.constant = 0;
        countWidth.constant = 60;
        _history = [[DataManager sharedInstance] getCountHistory];
    }
    
    
    [self addAdMob];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - AdMob

- (void)addAdMob
{
    bannerView.adUnitID = @"ca-app-pub-2783299302427084/5729753656";
    bannerView.rootViewController = self;
    
    GADRequest *request = [GADRequest request];
    [bannerView loadRequest:request];
}


#pragma mark - Button Actions
- (IBAction)backButtonTouched:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)dateButtonTouched:(id)sender
{
    [[DataManager sharedInstance] setHistoryType:HistoryTypeCount];
    [self refresh];
}

- (IBAction)countButtonTouched:(id)sender
{
    [[DataManager sharedInstance] setHistoryType:HistoryTypeDate];
    [self refresh];
}

#pragma mark - 

- (void)refresh
{
    if ([[DataManager sharedInstance] historyType] == HistoryTypeDate) {
        [UIView animateWithDuration:.5f animations:^{
            dateWidth.constant = 60;
            countWidth.constant = 0;
            [self.view layoutIfNeeded];
        }];
        _history = [[DataManager sharedInstance] getDateHistory];
    }else{
        [UIView animateWithDuration:.5f animations:^{
            dateWidth.constant = 0;
            countWidth.constant = 60;
            [self.view layoutIfNeeded];
        }];
        _history = [[DataManager sharedInstance] getCountHistory];
    }
    [_historyTableView setContentOffset:CGPointZero animated:NO];
    [_historyTableView reloadData];
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _history.allKeys.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([HistoryTableViewCell class])];
    
    NSString *key = [self convertedAllKeys][indexPath.row];
    
    [cell setMainText:[[DataManager sharedInstance] simPleKeyFromFullKey:key] subText:[_history[key] stringValue]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        NSLog(@"삭제하자~~~~~~~~~");
        NSString *removeKey = [self convertedAllKeys][indexPath.row];
        [[DataManager sharedInstance] removeHistoryWithKey:removeKey];
    }
    [self refresh];
}

- (NSArray *)convertedAllKeys
{
    NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending: NO];
    NSArray *keys = [_history.allKeys sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]];
    return keys;
}

@end
