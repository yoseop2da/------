//
//  MainViewController.m
//  PushUpTracker
//
//  Created by parkyoseop on 2016. 3. 2..
//  Copyright © 2016년 yoseop2da. All rights reserved.
//

#import "MainViewController.h"
#import "NSDate+Utilities.h"

#import "HistoryViewController.h"

@import AVFoundation;
@import GoogleMobileAds;

@interface MainViewController ()
{
    float _time;
    NSTimer *_timer;
    
    IBOutlet UIButton *tipButton;
    IBOutlet UIView *tipView;
    IBOutlet UILabel *guideLabel;
    IBOutlet NSLayoutConstraint *startWidth;
    IBOutlet NSLayoutConstraint *pauseWidth;
    IBOutlet NSLayoutConstraint *historyWidth;
    IBOutlet NSLayoutConstraint *historyLeadingWidth;
    
    IBOutlet GADBannerView *bannerView;
    
    IBOutlet UILabel *guideFirstLabel;
    IBOutlet UILabel *guideSecondLabel;
    IBOutlet UILabel *guideThirdLabel;
    
    IBOutlet UIImageView *firstImage;
    IBOutlet UIImageView *secondImage;
    IBOutlet UIImageView *thirdImage;
}
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UIButton *refreshButton;
@property (weak, nonatomic) IBOutlet UIButton *historyButton;

@property (weak, nonatomic) IBOutlet UIButton *soundButton;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    guideLabel.layer.cornerRadius = 10.f;
    guideLabel.clipsToBounds = YES;
    guideLabel.hidden = YES;
    
    [self initialize];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isAlreadyLaunch"]){
        tipView.alpha = 0.f;
    }else{
        [UIView animateWithDuration:.5f animations:^{
            tipButton.alpha = 1.f;
        }];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isAlreadyLaunch"];
    }
    
    firstImage.layer.cornerRadius = 21.f;
    firstImage.clipsToBounds = YES;
    firstImage.layer.borderColor = [UIColor blueColor].CGColor;
    
    secondImage.layer.cornerRadius = 21.f;
    secondImage.clipsToBounds = YES;
    secondImage.layer.borderColor = [UIColor blueColor].CGColor;
    
    thirdImage.layer.cornerRadius = 21.f;
    thirdImage.clipsToBounds = YES;
    thirdImage.layer.borderColor = [UIColor blueColor].CGColor;
    
    [self addAdMob];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - AdMob

- (void)addAdMob
{
    bannerView.adUnitID = @"ca-app-pub-2783299302427084/5729753656";
    bannerView.rootViewController = self;
    
    GADRequest *request = [GADRequest request];
    [bannerView loadRequest:request];
}

#pragma mark - Initialize

- (void)initialize
{
    _time = 0;
    [self setTimeLabel];
    [self setLabelFromCount:[[CountChecker sharedInstance] getCount]];
    
    startWidth.constant = 60;
    pauseWidth.constant = 0;
    
    [UIView animateWithDuration:.5f animations:^{
        _refreshButton.alpha = 0.f;
    }];
}

- (void)setTimeLabel
{
    int min = (int)_time/60;
    int sec = (int)_time%60;
    int millSec = ((int)((_time - min*60)*100))%60;
    
    _timeLabel.text = [NSString stringWithFormat:@"%02d:%02d.%.2d",min,sec,millSec];
}

- (void)setLabelFromCount:(int)count
{
    _countLabel.text = [@(count) stringValue];
}

#pragma mark - Button Actions
- (IBAction)tipButtonTouched:(UIButton *)button
{
    [UIView animateWithDuration:0.5 animations:^{
        tipView.alpha = ([self isTipViewOn])? 0.f:1.f;
    }completion:^(BOOL finished) {
        ;
        [self firstAction];
    }];
    
    
}

- (IBAction)startButtonTouched:(UIButton *)button
{
    [self start];
}

- (IBAction)pauseButtonTouched:(UIButton *)button
{
    [self pause];
}

- (IBAction)refreshButtonTouched:(id)sender
{
    if ([[CountChecker sharedInstance] getCount] > 0) {
        [[DataManager sharedInstance] saveHistoryWithCount:[[CountChecker sharedInstance] getCount]];
    }
    [self refresh];
}

- (IBAction)soundButtonTouched:(id)sender
{
    if ([self isSoundOn]) {
        _soundButton.alpha = 0.3;
    }else{
        _soundButton.alpha = 1.f;
    }
}

- (IBAction)historyButtonTouched:(id)sender
{
    HistoryViewController *historyViewController = [SharedAppDelegate.mainStoryboard instantiateViewControllerWithIdentifier:NSStringFromClass([HistoryViewController class])];
    [self.navigationController pushViewController:historyViewController animated:YES];
}

#pragma mark - Timer

- (void)startTimer
{
    [self invalidateTimer];
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.09 target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
}

- (void)invalidateTimer
{
    if ([_timer isValid]) {
        [_timer invalidate];
        _timer = nil;
    }
}

- (void)timerAction
{
    if(startWidth.constant == 0){// && [[CountChecker sharedInstance] getCount] <= 5){
//        if ((int)(_time*100)%2 == 0) {
//            guideLabel.hidden = !guideLabel.hidden;
//        }
        guideLabel.hidden = NO;
    }else{
        guideLabel.hidden = YES;
    }
    
    if (_time > 3600) {
        // 종료...
        [self invalidateTimer];
        _time = 3600;
        [self setTimeLabel];
        return;
    }
    
    _time = _time + 0.09;
    
    [self setTimeLabel];
}

#pragma mark - 

- (void)start
{
    [UIView animateWithDuration:.5f animations:^{
        startWidth.constant = 0;
        pauseWidth.constant = 60;
        
        historyWidth.constant = 0;
        historyLeadingWidth.constant = 0;
        
        [self.view layoutIfNeeded];
    }];
    
    [UIView animateWithDuration:.5f animations:^{
        _refreshButton.alpha = 0.f;
        tipButton.alpha = 0.f;
    }];
    
    [self startTimer];
    [self detectProximitySensor];
}

- (void)pause
{
    guideLabel.hidden = YES;
    
    [UIView animateWithDuration:.5f animations:^{
        startWidth.constant = 60;
        pauseWidth.constant = 0;
        
        historyWidth.constant = 60;
        historyLeadingWidth.constant = 10;
        
        [self.view layoutIfNeeded];
    }];
    
    [UIView animateWithDuration:.5f animations:^{
        _refreshButton.alpha = 1.f;
        tipButton.alpha = 1.f;
    }];
    
    [self invalidateTimer];
    [self removeProximitySensor];
}

- (void)refresh
{
    _time = 0;
    [self invalidateTimer];
    
    [[CountChecker sharedInstance] resetCount];
    [self initialize];
    [self removeProximitySensor];
}


#pragma mark - 근접센서 활용하기
- (void)detectProximitySensor
{
    [[UIDevice currentDevice] setProximityMonitoringEnabled:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkUpSensor) name:UIDeviceProximityStateDidChangeNotification object:nil];
}

- (void)removeProximitySensor
{
    [[UIDevice currentDevice] setProximityMonitoringEnabled:NO];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceProximityStateDidChangeNotification object:nil];
}

- (void)checkUpSensor
{
    if ([[UIDevice currentDevice] proximityState]) {
        
        [[CountChecker sharedInstance] addCount];
        NSLog(@"-------------------------- 접근 센서[%d]",[[CountChecker sharedInstance] getCount]);
        [self setLabelFromCount:[[CountChecker sharedInstance] getCount]];
        
        if ([self isSoundOn]) {
            AVSpeechUtterance *utterance = [AVSpeechUtterance speechUtteranceWithString:[@([[CountChecker sharedInstance] getCount]) stringValue]];
            AVSpeechSynthesizer *syn = [[AVSpeechSynthesizer alloc] init];
            [syn speakUtterance:utterance];
        }
    }
}

-(BOOL)isSoundOn
{
    return (_soundButton.alpha == 1.f);
}

-(BOOL)isTipViewOn
{
    return (tipView.alpha == 1.f);
}

#pragma mark - Tips Animation

- (void)firstAction
{
    guideFirstLabel.font = [UIFont fontWithName:@"AppleSDGothicNeo-Bold" size:19];
    guideSecondLabel.font = [UIFont fontWithName:@"AppleSDGothicNeo-UltraLight" size:17];
    guideThirdLabel.font = [UIFont fontWithName:@"AppleSDGothicNeo-UltraLight" size:17];
    
    firstImage.layer.borderWidth = 4.f;
    secondImage.layer.borderWidth = 0.f;
    thirdImage.layer.borderWidth = 0.f;
    
    [self performSelector:@selector(secondAction) withObject:nil afterDelay:2.f];
}

- (void)secondAction
{
    guideFirstLabel.font = [UIFont fontWithName:@"AppleSDGothicNeo-UltraLight" size:17];
    guideSecondLabel.font = [UIFont fontWithName:@"AppleSDGothicNeo-Bold" size:19];
    guideThirdLabel.font = [UIFont fontWithName:@"AppleSDGothicNeo-UltraLight" size:17];
    
    firstImage.layer.borderWidth = 0.f;
    secondImage.layer.borderWidth = 4.f;
    thirdImage.layer.borderWidth = 0.f;
    
    [self performSelector:@selector(thirdAction) withObject:nil afterDelay:1.5f];
}

- (void)thirdAction
{
    guideFirstLabel.font = [UIFont fontWithName:@"AppleSDGothicNeo-UltraLight" size:17];
    guideSecondLabel.font = [UIFont fontWithName:@"AppleSDGothicNeo-UltraLight" size:17];
    guideThirdLabel.font = [UIFont fontWithName:@"AppleSDGothicNeo-Bold" size:19];
    
    firstImage.layer.borderWidth = 0.f;
    secondImage.layer.borderWidth = 0.f;
    thirdImage.layer.borderWidth = 4.f;
    
    [self performSelector:@selector(defaultAction) withObject:nil afterDelay:1.5f];
}

- (void)defaultAction
{
    guideFirstLabel.font = [UIFont fontWithName:@"AppleSDGothicNeo-UltraLight" size:17];
    guideSecondLabel.font = [UIFont fontWithName:@"AppleSDGothicNeo-UltraLight" size:17];
    guideThirdLabel.font = [UIFont fontWithName:@"AppleSDGothicNeo-UltraLight" size:17];
    
    firstImage.layer.borderWidth = 0.f;
    secondImage.layer.borderWidth = 0.f;
    thirdImage.layer.borderWidth = 0.f;
}

@end
