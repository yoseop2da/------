//
//  RoundButton.m
//  TodaySentence
//
//  Created by yoseop on 2015. 12. 12..
//  Copyright © 2015년 Yoseop Park. All rights reserved.
//

#import "RoundButton.h"

@implementation RoundButton

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        self.layer.cornerRadius = 30.f;
    }
    return self;
}

@end
